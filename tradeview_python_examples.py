from pprint import pprint
import requests

headers={'x-api-key': YOUR_API_KEY}
endpoint = 'validate-fpml-xml'
url = f"https://tradeviewapi.finaprins.com/{endpoint}"  
file_path = 'https://tradeviewapi.finaprins.com/public-test-data/fpml/bond-option.xml'
file_content = requests.get(file_path).content
response = requests.request('POST', url, params=None, files=dict(xml_file=file_content), headers=headers)
if response:
    pprint(response.json())
